#ifndef AFFICHAGE_H
#define AFFICHAGE_H

#include <iostream>
#include <libpq-fe.h>

/** \brief Affichage de l'entete de la base de donnée
 *
 * Permet de afficher l'entete de la base de donnée avec les espaces en fonction du champ le plus grands
 *
 *\param connexion \c PGresult* Requête sql de la BDD
 *\param caractere \c unsigned \c int Nombres espaces à mettre après le champ
 *\param colonnes \c unsigned \c int Nombres de champ en fonction de la requete
 */
void affichage_entete(PGresult *connexion, unsigned int caractere, unsigned int colonnes);
/** \brief Affichage des données de la base de donnée
 *
 * Permet de afficher les données de la base de donnée avec les espaces en fonction du champ le plus grands
 *
 *\param connexion \c PGresult* Requête sql de la BDD
 *\param caractere \c unsigned \c int Nombres espaces à mettre après le champ
 *\param colonnes \c unsigned \c int Nombres de champ en fonction de la requête
 *\param result \c unsigned \c int Nombres de données en fonction de la requete
 */
void affichage_donnees(PGresult *connexion, unsigned int caractere, unsigned int colonnes, unsigned int result);
/** \brief Affichage des tirets entre les données
 *
 * Permet afficher les tirets entres les données en fonction de la taille des champs
 *
 *\param tiret \c unsigned \c int Nombre de tiret à afficher
 */
void affichage_tiret(unsigned int);
/** \brief Affichage des erreurs
 *
 * Permet afficher les erreurs si la connexion à la base de donnée échoue
 *
 *\param reussite \c ExecStatusType Status de la connexion à la base de donnée
 *\param status_requete \c char* Affichage du status de la requête
 */
void AfficheErreur(ExecStatusType reussite, char *status_requete);
/** \brief Affichage des informations de connexion
 *
 * Affichage de la connexion à la base de donnée
 *
 *\param connexion \c PGconn* Requête SQL
 */
void AfficheInfos(PGconn *);

#endif // AFFICHAGE_H

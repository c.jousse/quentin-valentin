#include <iostream>
#include <libpq-fe.h>
#include "calcul.h"
#include <cstring>
#include <string>
#include <iomanip>

using namespace std;


std::string supprimmeChaine(char *chaine, unsigned int maxCaractere)
{
  std::string output;

  if(std::strlen(chaine) > maxCaractere)
  {
    for(unsigned int t = 0; t < maxCaractere - 3; t++)
    {
      output.push_back(chaine[t]);
    }

    output.append("...");
  }
  else
  {
    output.append(chaine);
  }

  return output;
}


unsigned int calculNombreCaractere(unsigned int nbCaractere, unsigned int nbrColonnes, PGresult *requetes)
{
  for(unsigned int champ = 0; champ < nbrColonnes; champ++)
  {
    unsigned int longueurChamp = std::strlen(PQfname(requetes, champ));

    if(longueurChamp > nbCaractere)
    {
      nbCaractere = longueurChamp;
    }
  }

  return nbCaractere;
}


unsigned int separationTiret(unsigned int nbrColonnes, unsigned int nbSeparation, unsigned int nbCaractere, PGresult *requetes)
{
  for(unsigned int p = 0; p < nbrColonnes; p++)
  {
    unsigned int champVide = nbCaractere - std::strlen(PQfname(requetes, p));
    nbSeparation = nbSeparation + champVide + 3 + std::strlen(PQfname(requetes, p));
  }

  return nbSeparation;
}

